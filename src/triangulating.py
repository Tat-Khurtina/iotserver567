import json
import random

# starting_coords = [(0, 0, 1), (0, 1, 0), (1, 0, 0)]
coords_to_match = []


# destinations = []
# (1, 2, 2), (4, 4, 6), (7, 2, 3))

class Triangulation:
    def __init__(self, destinations=None, starting_coords=None, point=None):

        if not (destinations or starting_coords or point):
            Exception("Укажите хотя бы одно поле!")

        self.destinations = [float(x) for x in destinations.split(",")] if type(destinations) is str else None

        self.starting_coords = [[float(x) for x in y.split(",")] for y in starting_coords.split(';')] \
            if type(starting_coords) is str \
            else starting_coords or ((1, 0, 0), (1, 0, 1), (1, 1, 1)) #7
            #else starting_coords or ((1, 0, 0), (1, 1, 0), (1, 1, 1)) #6
            #else starting_coords or ((1, 0, 0), (0, 1, 0), (1, 0, 1)) #5
            #else starting_coords or ((1, 0, 0), (0, 1, 0), (0, 0, 1))

        self.x, self.y, self.z = [float(x) for x in point.split(",")] \
            if type(point) is str \
            else [None for x in range(3)]

        if not (self.x and self.y and self.z):
            self.get_coords()

    def __get_f(self):
        return (self.destinations[1] ** 2 - self.destinations[0] ** 2 - self.starting_coords[1][0] ** 2 +
                self.starting_coords[0][0] ** 2 -
                self.starting_coords[1][1] ** 2 + self.starting_coords[0][1] ** 2 -
                self.starting_coords[1][2] ** 2 + self.starting_coords[0][2] ** 2) / 2

    def __get_f5(self):
        return (self.destinations[1] ** 2 - self.destinations[0] ** 2 - self.starting_coords[1][0] ** 2 +
                self.starting_coords[0][0] ** 2 -
                self.starting_coords[1][1] ** 2 + self.starting_coords[0][1] ** 2 -
                self.starting_coords[1][2] ** 2 + self.starting_coords[0][2] ** 2) / (2 * (self.starting_coords[0][0] - self.starting_coords[1][0]))

    def __get_f6(self):
        return (self.destinations[1] ** 2 - self.destinations[0] ** 2 -
                (self.starting_coords[1][1] ** 2 - self.starting_coords[0][1] ** 2) -
                (self.starting_coords[1][2] ** 2 - self.starting_coords[0][2] ** 2)) / (2 *
                (self.starting_coords[0][1] - self.starting_coords[1][1]))

    def __get_f7(self):
        return (self.destinations[1] ** 2 - self.destinations[0] ** 2 -
                self.starting_coords[1][2] ** 2 + self.starting_coords[0][2] ** 2) / (2 *
                (self.starting_coords[0][2] - self.starting_coords[1][2]))

    def __get_d(self):
        return (self.starting_coords[0][1] - self.starting_coords[1][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])

    def __get_d5(self):
        return (self.starting_coords[0][1] - self.starting_coords[1][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])

    def __get_d6(self):
        return (self.starting_coords[0][2] - self.starting_coords[1][2]) / (
                    self.starting_coords[0][1] - self.starting_coords[1][1])

    def __get_e(self):
        return (self.starting_coords[0][2] - self.starting_coords[1][2]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])

    def __get_e5(self):
        return (self.starting_coords[0][2] - self.starting_coords[1][2]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])

    def __get_e6(self):
        return (self.starting_coords[0][2] - self.starting_coords[2][2]) / (
                    self.starting_coords[0][1] - self.starting_coords[2][1])

    def __get_e7(self):
        return self.__get_h7() - self.__get_g7() * self.__get_f7()



    def __get_g(self):
        a = (self.starting_coords[0][2] - self.starting_coords[2][2]) / (
                    self.starting_coords[0][0] - self.starting_coords[2][0])
        b = (self.starting_coords[0][2] - self.starting_coords[1][2]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])
        c = (self.starting_coords[0][1] - self.starting_coords[2][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[2][0])
        d = (self.starting_coords[0][1] - self.starting_coords[1][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])

        return (a - b) / (c - d)

    def __get_g6(self):
        return (self.destinations[2] ** 2 - self.destinations[0] ** 2 -
                (self.starting_coords[2][1] ** 2 - self.starting_coords[0][1] ** 2) -
                (self.starting_coords[2][2] ** 2 - self.starting_coords[0][2] ** 2)) / (2 *
                (self.starting_coords[0][1] - self.starting_coords[2][1]))

    def __get_g7(self):
        return (self.starting_coords[0][2] - self.starting_coords[2][2]) / (
                (self.starting_coords[0][1] - self.starting_coords[2][1]))

    def __get_h(self):
        a = self.destinations[2] ** 2 - self.destinations[1] ** 2 + self.starting_coords[1][0] ** 2 - \
            self.starting_coords[2][0] ** 2 + \
            self.starting_coords[1][1] ** 2 \
            - self.starting_coords[2][1] ** 2 + self.starting_coords[1][2] ** 2 - self.starting_coords[2][2] ** 2
        b = (self.starting_coords[0][1] - self.starting_coords[2][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[2][0])  # -20/-6
        c = (self.starting_coords[0][1] - self.starting_coords[1][1]) / (
                    self.starting_coords[0][0] - self.starting_coords[1][0])  # -10/-3

        return a / 2 * ((b - c) or 1)

    def __get_h5(self):
        return (self.destinations[2] ** 2 - self.destinations[1] ** 2 - self.starting_coords[2][2] ** 2 + \
            self.starting_coords[0][2] ** 2) / (2 * (self.starting_coords[0][2] - self.starting_coords[2][2]))

    def __get_h7(self):
        return (self.destinations[2] ** 2 - self.destinations[0] ** 2 - self.starting_coords[2][1] ** 2 + \
            self.starting_coords[0][1] ** 2 - self.starting_coords[2][2] ** 2 + self.starting_coords[0][2] ** 2
                ) / (2 * (self.starting_coords[0][1] - self.starting_coords[2][1]))

    def __get_p(self):
        return self.__get_f() - self.__get_d() * self.__get_h()

    def __get_p5(self):
        return self.__get_f5() - self.__get_e5() * self.__get_h5()

    def __get_p6(self):
        return (self.__get_f6() - self.__get_g6()) / (self.__get_d6() - self.__get_e6())

    def __get_q(self):
        return self.__get_e() - self.__get_d() * self.__get_g()

    def __get_q6(self):
        return (self.__get_d6() * self.__get_g6() - self.__get_f6() * self.__get_e6()) / (
                self.__get_d6() - self.__get_e6())

    def __get_a(self):
        return self.__get_q() ** 2 + self.__get_g() ** 2 + 1

    def __get_a5(self):
        return self.__get_d5() ** 2 + 1

    def __get_a6(self):
        return 1

    def __get_a7(self):
        return 1

    def __get_b(self):
        return 2 * self.__get_q() * self.starting_coords[0][0] - 2 * self.__get_p() * self.__get_q() \
               - 2 * self.__get_h() * self.__get_g() + 2 * self.__get_g() * self.starting_coords[0][1] - 2 * \
               self.starting_coords[0][2]

    def __get_b5(self):
        return 2 * self.__get_p5() * self.starting_coords[0][0] - 2 * self.__get_p5() * self.__get_d5() \
               - 2 * self.starting_coords[0][2]

    def __get_b6(self):
        return -2 * self.starting_coords[0][0]

    def __get_b7(self):
        return -2 * self.starting_coords[0][0]

    def __get_c(self):
        # return self.__get_p() ** 2 - 2 * self.__get_p() * self.starting_coords[0][0] + self.starting_coords[0][0]**2 \
        #         + self.__get_h() ** 2 - 2 * self.__get_h() * self.starting_coords[0][1] + self.starting_coords[0][1]**2 \
        #         + self.starting_coords[0][2] ** 2 - self.destinations[0] ** 2
        return (self.__get_p() - self.starting_coords[0][0]) ** 2 + (self.__get_h() - self.starting_coords[0][1]) ** 2 \
               + self.starting_coords[0][2] ** 2 - self.destinations[0] ** 2

    def __get_c5(self):
        return self.__get_p5() ** 2 - 2 * self.__get_p5() * self.starting_coords[0][0] + self.starting_coords[0][0] ** 2 \
            + self.__get_h5() ** 2 - 2 * self.__get_h5() * self.starting_coords[0][2] + self.starting_coords[0][2] ** 2 \
            - self.destinations[0] ** 2

    def __get_c6(self):
        return self.starting_coords[0][0] ** 2 + self.__get_p6() ** 2 - 2 * self.__get_p6() * self.starting_coords[0][1] + \
               self.starting_coords[0][1] ** 2 + self.__get_q6() ** 2 - 2 * self.__get_q6() * self.starting_coords[0][2] + \
               self.starting_coords[0][2] ** 2 - self.destinations[0] ** 2

    def __get_c7(self):
        return self.starting_coords[0][0] ** 2 + self.__get_e7() ** 2 - 2 * self.__get_e7() * self.starting_coords[0][1] + \
               self.starting_coords[0][1] ** 2 + self.__get_f7() ** 2 - 2 * self.__get_f7() * self.starting_coords[0][2] + \
               self.starting_coords[0][2] ** 2 - self.destinations[0] ** 2

    def get_coords(self):
        # случай 5
        if (self.starting_coords[0][0] != self.starting_coords[1][0] and self.starting_coords[0][0] == self.starting_coords[2][0] and
            self.starting_coords[0][1] == self.starting_coords[2][1] and self.starting_coords[0][2] != self.starting_coords[2][2]):
            print("Случай ",5)

            d = self.__get_b5() ** 2 - 4 * self.__get_a5() * self.__get_c5()

            if d < 0:
                return f"Нелья посчитать значение для d = {d}"
            elif d == 0:
                self.x = self.__get_p5() - self.__get_d5() * (-self.__get_b5() + d) / (2 * self.__get_a5()),

                self.y = (-self.__get_b5() + d) / (2 * self.__get_a5()),

                self.z = self.__get_h5(),

                return self.x, self.y, self.z

            else:
                # print(d)

                d = d ** (1 / 2)

                x_plus = self.__get_p5() - self.__get_d5() * (-self.__get_b5() + d) / (2 * self.__get_a5())
                x_minus = self.__get_p5() - self.__get_d5() * (-self.__get_b5() - d) / (2 * self.__get_a5())

                y_plus = (-self.__get_b5() + d) / (2 * self.__get_a5())
                y_minus = (-self.__get_b5() - d) / (2 * self.__get_a5())


                z_plus = self.__get_h5()
                z_minus = self.__get_h5()

                self.x = x_plus, x_minus
                self.y = y_plus, y_minus
                self.z = z_plus, z_minus

        # случай 6
        elif (self.starting_coords[0][0] == self.starting_coords[2][0] and self.starting_coords[0][0] == self.starting_coords[1][0] and
            self.starting_coords[0][1] != self.starting_coords[1][1] and self.starting_coords[0][2] != self.starting_coords[2][2]):
            print("Случай ",6)

            d = self.__get_b6() ** 2 - 4 * self.__get_a6() * self.__get_c6()

            if d < 0:
                return f"Нелья посчитать значение для d = {d}"
            elif d == 0:
                self.x = (-self.__get_b6() + d) / (2 * self.__get_a6()),

                self.y = self.__get_q6(),

                self.z = self.__get_p6(),

                return self.x, self.y, self.z

            else:
                # print(d)

                d = d ** (1 / 2)

                x_plus = (-self.__get_b6() + d) / (2 * self.__get_a6())
                x_minus = (-self.__get_b6() - d) / (2 * self.__get_a6())

                y_plus = self.__get_q6()
                y_minus = self.__get_q6()


                z_plus = self.__get_p6()
                z_minus = self.__get_p6()

                self.x = x_plus, x_minus
                self.y = y_plus, y_minus
                self.z = z_plus, z_minus

        # случай 7
        elif (self.starting_coords[0][0] == self.starting_coords[1][0] and self.starting_coords[0][0] == self.starting_coords[2][0] and
            self.starting_coords[0][1] == self.starting_coords[1][1] and self.starting_coords[0][1] != self.starting_coords[2][1] and
            self.starting_coords[0][2] != self.starting_coords[1][2]):
            print("Случай ",7)

            d = self.__get_b7() ** 2 - 4 * self.__get_a7() * self.__get_c7()

            if d < 0:
                return f"Нелья посчитать значение для d = {d}"
            elif d == 0:
                self.x = (-self.__get_b7() + d) / (2 * self.__get_a7()),

                self.y = self.__get_e7(),

                self.z = self.__get_f7(),

                return self.x, self.y, self.z

            else:
                # print(d)

                d = d ** (1 / 2)

                x_plus = (-self.__get_b7() + d) / (2 * self.__get_a7())
                x_minus = (-self.__get_b7() - d) / (2 * self.__get_a7())

                y_plus = self.__get_e7()
                y_minus = self.__get_e7()


                z_plus = self.__get_f7()
                z_minus = self.__get_f7()

                self.x = x_plus, x_minus
                self.y = y_plus, y_minus
                self.z = z_plus, z_minus



        else:
            print("без случ")
            d = self.__get_b() ** 2 - 4 * self.__get_a() * self.__get_c()

            if d < 0:
                return f"Нелья посчитать значение для d = {d}"
            elif d == 0:
                self.x = self.__get_p() - self.__get_q() * (-self.__get_b() + d) / (2 * self.__get_a()),

                self.y = self.__get_h() - self.__get_g() * (-self.__get_b() + d) / (2 * self.__get_a()),

                self.z = (-self.__get_b() + d) / (2 * self.__get_a()),

                return self.x, self.y, self.z

            else:
                # print(d)

                d = d ** (1 / 2)

                x_plus = self.__get_p() - self.__get_q() * (-self.__get_b() + d) / (2 * self.__get_a())
                x_minus = self.__get_p() - self.__get_q() * (-self.__get_b() - d) / (2 * self.__get_a())

                y_plus = self.__get_h() - self.__get_g() * (-self.__get_b() + d) / (2 * self.__get_a())
                y_minus = self.__get_h() - self.__get_g() * (-self.__get_b() - d) / (2 * self.__get_a())

                # print(-self.__get_b(), d, 2 * self.__get_a())

                z_plus = (-self.__get_b() + d) / (2 * self.__get_a())
                z_minus = (-self.__get_b() - d) / (2 * self.__get_a())

                self.x = x_plus, x_minus
                self.y = y_plus, y_minus
                self.z = z_plus, z_minus

        return (x_plus, x_minus), (y_plus, y_minus), (z_plus, z_minus)

    def __get_destinations(self):
        return [[((coord[0] - self.x[x]) ** 2 +
                  (coord[1] - self.y[x]) ** 2 +
                  (coord[2] - self.z[x]) ** 2) ** 0.5 for coord in self.starting_coords] for x in range(len(self.x))]

    def __random_distance(self):
        return random.random() / 2 * [-1, 1][int(random.random())]

    def move(self, iters):

        point_coordinates = {0: {"x": self.x,
                                 "y": self.y,
                                 "z": self.z,
                                 "destintations": self.destinations}}

        for step in range(iters):
            dx, dy, dz = [self.__random_distance() for x in range(3)]
            # print(dx, dy, dz)

            self.x = [x + dx for x in self.x]
            self.y = [y + dy for y in self.y]
            self.z = [z + dz for z in self.z]

            point_coordinates[step + 1] = {
                "x": self.x,
                "y": self.y,
                "z": self.z,
                "destinations": self.__get_destinations()
            }

        return point_coordinates

    def get_distances(self, point=None):
        # distances = distances or self.starting_coords
        point = point or (self.x, self.y, self.z)
        # p = [float(x) for x in distances.split(",")]
        if len(point) < 3:
            return f"Неверный ввод данных"

        distances = [((coord[0] - point[0]) ** 2 +
                      (coord[1] - point[1]) ** 2 +
                      (coord[2] - point[2]) ** 2) ** 0.5 for coord in self.starting_coords]

        return distances  # rez[0], rez[1], rez[2]

# if type(self.x) is not list:
#     self.x += dx
#     self.y += dy
#     self.z += dz
#
#     point_coordinates[step + 1] = {
#         "x": self.x,
#         "y": self.y,
#         "z": self.z,
#         "destinations": self.__get_destinations()
#     }
# else: