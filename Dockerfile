FROM python:3

WORKDIR /home/bitnami/server

COPY . .

RUN pip install Flask

EXPOSE 1337

CMD ["python", "server.py"]