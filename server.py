from flask import Flask
from flask import request
from src.triangulating import Triangulation
import json
app = Flask(__name__)


@app.route("/")
def index():
    return "running!"


@app.route('/get_coordinates/<destinations>')
def get_coordinates(destinations=None):
    print(destinations)
    response = Triangulation(destinations).get_coords()

    if type(response) is str:
        return json.dumps({
            "status": 500,
            "response": response
        })
    else:
        return json.dumps({
            "status": 200,
            "response": {
                "x": response[0],
                "y": response[1],
                "z": response[2]
            }
        })


@app.route("/move/<destinations>")
def get_moving_positions(destinations=None):
    steps = int(request.args.get("iters") or 1)
    print(steps)

    coordinates = Triangulation(destinations).move(steps)

    return json.dumps(coordinates)


@app.route("/get_distances/<p_point>")
def get_distances(p_point=None):
    points = request.args.get("points")

    response = Triangulation(point=p_point, starting_coords=points).get_distances()

    if type(response) is str:
        return json.dumps({
            "status": 500,
            "response": response
        })
    else:
        return json.dumps({
            "status": 200,
            "response": {
                "destinations": response
            }
        })


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5000)
    #app.run(host="0.0.0.0", port=1337)
